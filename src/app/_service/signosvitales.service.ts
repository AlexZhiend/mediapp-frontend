import { Injectable } from '@angular/core';
import { GenericService } from './generic.service';
import { SignosVitales } from '../_model/signosVitales';
import { Subject } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class SignosvitalesService extends GenericService<SignosVitales>{

  private signosCambio = new Subject<SignosVitales[]>();
  private mensajeCambio = new Subject<string>();

  constructor(protected http: HttpClient) {
    super(http,`${environment.HOST}/signosvitales`);
  }

    listarPageable(p: number, s:number){
      return this.http.get<any>(`${this.url}/pageable?page=${p}&size=${s}`);
    }

    //get Subjects
    getSignosVitalesCambio() {
      return this.signosCambio.asObservable();
    }

    getMensajeCambio() {
      return this.mensajeCambio.asObservable();
    }

    //set Subjects
    setSignosVitalesCambio(signosvitales: SignosVitales[]) {
      this.signosCambio.next(signosvitales);
    }

    setMensajeCambio(mensaje: string) {
      this.mensajeCambio.next(mensaje);
    }
}
