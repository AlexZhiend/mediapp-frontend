import { Paciente } from './paciente';
export class SignosVitales{
  idsignosvitales: number;
  fecha: string;
  temperatura: String;
  pulso: String;
  ritmoRespiratorio: String;
  paciente:Paciente;
}
