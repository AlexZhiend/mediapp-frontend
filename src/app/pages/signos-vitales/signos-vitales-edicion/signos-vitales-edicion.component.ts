import { Component, OnInit } from '@angular/core';
import { SignosVitales } from '../../../_model/signosVitales';
import { FormGroup, FormControl } from '@angular/forms';
import { SignosvitalesService } from '../../../_service/signosvitales.service';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { switchMap, map } from 'rxjs/operators';
import { Paciente } from '../../../_model/paciente';
import { Observable } from 'rxjs';
import { PacienteService } from '../../../_service/paciente.service';

@Component({
  selector: 'app-signos-vitales-edicion',
  templateUrl: './signos-vitales-edicion.component.html',
  styleUrls: ['./signos-vitales-edicion.component.css']
})
export class SignosVitalesEdicionComponent implements OnInit {

  id: number;
  signosVitales: SignosVitales;
  form: FormGroup;
  edicion: boolean = false;
  maxFecha: Date = new Date();
  pacientes: Paciente[];
  pacienteSeleccionado: Paciente;



  myControlPaciente: FormControl = new FormControl();

  pacientesFiltrados$: Observable<Paciente[]>;


  constructor(
    private signosvitalesService: SignosvitalesService,
    private route: ActivatedRoute,
    private router: Router,
    private pacienteService: PacienteService,
    ) { }

  ngOnInit(): void {
    this.signosVitales = new SignosVitales();

    this.form = new FormGroup({
      'id': new FormControl(0),
      'fecha': new FormControl(new Date()),
      'pulso': new FormControl(''),
      'temperatura': new FormControl(''),
      'ritmoRespiratorio': new FormControl(''),
      'paciente': this.myControlPaciente,
    });

    this.route.params.subscribe((params: Params) => {
      this.id = params['id'];
      this.edicion = params['id'] != null;
      this.initForm();
    });

    this.listarPacientes();

    this.pacientesFiltrados$ = this.myControlPaciente.valueChanges.pipe(map(val =>
      this.filtrarPacientes(val)));

  }

  filtrarPacientes(val: any) {
    if (val != null && val.idPaciente > 0) {
      return this.pacientes.filter(el =>
        el.nombres.toLowerCase().includes(val.nombres.toLowerCase()) || el.apellidos.toLowerCase().includes(val.apellidos.toLowerCase()) || el.dni.includes(val.dni)
      );
    }
    return this.pacientes.filter(el =>
      el.nombres.toLowerCase().includes(val?.toLowerCase()) || el.apellidos.toLowerCase().includes(val?.toLowerCase()) || el.dni.includes(val)
    );
  }

  initForm() {
    if (this.edicion) {
      this.signosvitalesService.listarPorId(this.id).subscribe(data => {

        this.form = new FormGroup({
          'id': new FormControl(data.idsignosvitales),
          'fecha': new FormControl(data.fecha),
          'pulso': new FormControl(data.pulso),
          'temperatura': new FormControl(data.temperatura),
          'ritmoRespiratorio': new FormControl(data.ritmoRespiratorio),
          'paciente': new FormControl(data.paciente)
        });
      });
    }
  }

  operar() {
    this.signosVitales.idsignosvitales = this.form.value['id'];
    this.signosVitales.fecha = this.form.value['fecha'];
    this.signosVitales.pulso = this.form.value['pulso'];
    this.signosVitales.temperatura = this.form.value['temperatura'];
    this.signosVitales.ritmoRespiratorio = this.form.value['ritmoRespiratorio'];
    this.signosVitales.paciente = this.form.value['paciente'];

    if (this.signosVitales != null && this.signosVitales.idsignosvitales > 0) {
      //BUENA PRACTICA
      this.signosvitalesService.modificar(this.signosVitales).pipe(switchMap(() => {
        return this.signosvitalesService.listar();
      })).subscribe(data => {
        this.signosvitalesService.setSignosVitalesCambio(data);
        this.signosvitalesService.setMensajeCambio("Se modificó");
      });

    } else {
      //PRACTICA COMUN
      console.log(this.signosVitales)
      this.signosvitalesService.registrar(this.signosVitales).subscribe(data => {
        this.signosvitalesService.listar().subscribe(signos => {
          this.signosvitalesService.setSignosVitalesCambio(signos);
          this.signosvitalesService.setMensajeCambio("Se registró");
        });
      });
    }

    this.router.navigate(['/pages/signosvitales']);
  }

  mostrarPaciente(val: any) {
    return val ? `${val.nombres} ${val.apellidos}` : val;
  }

  listarPacientes() {
    this.pacienteService.listar().subscribe(data => {
      this.pacientes = data;
    });
  }

}
