import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { SignosVitales } from '../../_model/signosVitales';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { SignosvitalesService } from '../../_service/signosvitales.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-signos-vitales',
  templateUrl: './signos-vitales.component.html',
  styleUrls: ['./signos-vitales.component.css']
})
export class SignosVitalesComponent implements OnInit {

  displayedColumns = ['id', 'fecha','pulso','temperatura','ritmo','paciente', 'acciones'];
  dataSource: MatTableDataSource<SignosVitales>;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(
    private signosvitalesService: SignosvitalesService,
    private snackBar: MatSnackBar,
    private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.signosvitalesService.getSignosVitalesCambio().subscribe(data => {
      this.dataSource = new MatTableDataSource(data);
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
    });

    this.signosvitalesService.getMensajeCambio().subscribe(data => {
      this.snackBar.open(data, 'Aviso', {
        duration: 2000,
      });
    });

    this.signosvitalesService.listar().subscribe(data => {
      this.dataSource = new MatTableDataSource(data);
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
    });
  }


  filtrar(e: any) {
    this.dataSource.filter = e.target.value.trim().toLowerCase();
  }

  eliminar(signosvitales: SignosVitales) {
    this.signosvitalesService.eliminar(signosvitales.idsignosvitales).pipe(switchMap(() => {
      return this.signosvitalesService.listar();
    })).subscribe(data => {
      this.signosvitalesService.setSignosVitalesCambio(data);
      this.signosvitalesService.setMensajeCambio('Se eliminó');
    });
  }


  verificarHijos(){
    return this.route.children.length !== 0;
  }

}
